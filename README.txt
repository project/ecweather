
Introduction
------------
This module shows a weather block and page, data from Environment Canada
  http://www.weatheroffice.gc.ca


Installation
------------
 * Enable the aggregator and ecweather modules
 * Add an aggregator feed /admin/content/aggregator/add/feed
 * To find the feed url
   * Go to http://www.weatheroffice.gc.ca
   * Choose the province and city
   * In the html source, look for rss
   * E.g. for Vancouver, BC the feed is 
     http://www.weatheroffice.gc.ca/rss/city/bc-74_e.xml
 * Add aggregator fid to /admin/settings/ecweather
 * Enable the block in /admin/build/block
 * Go to /ecweather to see forecast

TODO
----
 * automatically add/link to aggregator feed
